# vi:syntax=sh
# {{ ansible_managed }}
# $HOME/.bashrc
#
# My fucking Awesome BASHRC settings
###############################################################

#-------------------------------------------------------------#
# Bash version check                                          #
#-------------------------------------------------------------#
if (( BASH_VERSINFO[0] < 4 ))
then
  echo "bashrc: Looks like you're running an older version of Bash."
  echo "bashrc: You need at least bash-4.0 or some options will not work correctly."
  sleep 2
fi

#-------------------------------------------------------------#
# Interactive shell test                                      #
#-------------------------------------------------------------#
iatest=$(expr index "$-" i)

#-------------------------------------------------------------#
# Global definitions                                          #
#-------------------------------------------------------------#
# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

#-------------------------------------------------------------#
# Force bash completion                                       #
#-------------------------------------------------------------#
# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

#-------------------------------------------------------------#
# Colours                                                     #
#-------------------------------------------------------------#
# Enable 256 Colors
export TERM="xterm-256color"
# Enable 256 colors in tmux
[[ $TERM == "screen" ]] && export -p TERM="screen-256color"
# To have colors for ls and all grep commands such as grep, egrep and zgrep
export CLICOLOR=1
export LS_COLORS='no=00:fi=00:di=00;34:ln=01;36:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.gz=01;31:*.bz2=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.avi=01;35:*.fli=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.ogg=01;35:*.mp3=01;35:*.wav=01;35:*.xml=00;31:'
# ignore case, long prompt, exit if it fits on one screen, allow colors for ls and grep colors
export LESS="-iMFXR"

#-------------------------------------------------------------#
# Interactive Shell behaviour                                 #
# See https://github.com/mrzool/bash-sensible                 #
#-------------------------------------------------------------#
if [[ $iatest > 0 ]]
then
  # Turn the bell off
  bind "set bell-style visible"

  # check the window size after each command and, if necessary,
  # update the values of LINES and COLUMNS.
  shopt -s checkwinsize

  # Disable CTRL+d
  set -o ignoreeof

  # must press ctrl-D 2+1 times to exit shell
  export IGNOREEOF="2"

  # Prevent file overwrite on stdout redirection
  # Use `>|` to force redirection to an existing file
  set -o noclobber

  # Automatically trim long paths in the prompt (requires Bash 4.x)
  PROMPT_DIRTRIM=2

  # Enable history expansion with space
  # E.g. typing !!<space> will replace the !! with your last command
  bind Space:magic-space

  # Turn on recursive globbing (enables ** to recurse all directories)
  shopt -s globstar 2> /dev/null

  # Case-insensitive globbing (used in pathname expansion)
  shopt -s nocaseglob;

  ## SMARTER TAB-COMPLETION (Readline bindings) ##

  # Perform file completion in a case insensitive fashion
  bind "set completion-ignore-case on"

  # Treat hyphens and underscores as equivalent
  bind "set completion-map-case on"

  # Display matches for ambiguous patterns at first tab press
  bind "set show-all-if-ambiguous on"

  # Immediately add a trailing slash when autocompleting symlinks to directories
  bind "set mark-symlinked-directories on"

  ## BETTER DIRECTORY NAVIGATION ##

  # Prepend cd to directory names automatically
  shopt -s autocd 2> /dev/null

  # Correct spelling errors during tab-completion
  shopt -s dirspell 2> /dev/null

  # Correct spelling errors in arguments supplied to cd
  shopt -s cdspell 2> /dev/null

  # This defines where cd looks for targets
  # Add the directories you want to have fast access to, separated by colon
  # Ex: CDPATH=".:~:~/projects" will look for targets in the current working directory, in home and in the ~/projects folder
  CDPATH="."

  # This allows you to bookmark your favorite places across the file system
  # Define a variable containing a path and you will be able to cd into it regardless of the directory you're in
  shopt -s cdable_vars
fi

#-------------------------------------------------------------#
# History settings                                            #
# See https://sanctum.geek.nz/arabesque/better-bash-history/  #
#-------------------------------------------------------------#
# Append history instead of rewriting it
shopt -s histappend
# Allow a larger history file
HISTFILESIZE=1000000
HISTSIZE=1000000
# Don’t store duplicate lines
HISTCONTROL="erasedups:ignoreboth"
# Ignore certain commands
HISTIGNORE='bg:fg:history:ls:exit'
# Record timestamps
HISTTIMEFORMAT='%F %T '
# Use one command per line
shopt -s cmdhist
# Store history immediately
PROMPT_COMMAND='history -a'
# Enable incremental history search with up/down arrows (also Readline goodness)
# Learn more about this here: http://codeinthehole.com/writing/the-most-important-command-line-tip-incremental-history-searching-with-inputrc/
#bind '"\e[A": history-search-backward'
#bind '"\e[B": history-search-forward'
#bind '"\e[C": forward-char'
#bind '"\e[D": backward-char'
# allow ctrl-S for history navigation (with ctrl-R)
#stty -ixon

#-------------------------------------------------------------#
# Global Aliases                                              #
#-------------------------------------------------------------#
# To temporarily bypass an alias, we preceed the command with a \
# EG: the ls command is aliased, but to use the normal ls command you would type \ls

# DO WHAT I WANT!
alias fucking='sudo'

# Show disk space and space used in a folder
alias diskspace="du -S | sort -n -r |more"
alias folders='du -h --max-depth=1 | sort -h'
alias folderssort='find . -maxdepth 1 -type d -print0 | xargs -0 du -sk | sort -rn'
alias tree='tree -CAhF --dirsfirst'
alias treedir='tree -CAFd'
alias mountedinfo='df -hT'

# Show all logs
alias logs="sudo find /var/log -type f -exec file {} \; | grep 'text' | cut -d' ' -f1 | sed -e's/:$//g' | grep -v '[0-9]$'"

# Show open ports
alias openports='netstat -nape --inet'

# Count all files (recursively) in the current folder
alias countfiles='for t in files links directories; do echo $(find . -type ${t:0:1} | wc -l) $t; done'

# To see if a command is aliased, a file, or a built-in command
alias checkcommand='type -t'

# Search running processes
alias topcpu='/bin/ps -eo pcpu,pid,user,args | sort -k 1 -r | head -10'

# Bounce swap
alias bounce_swap='sudo swapoff -av && sleep 10 && sudo swapon -av'

# Multiple directory listing commands + colours!
if [ -x /usr/bin/dircolors ]; then
  test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
  alias ls='ls --color=auto'
fi
alias l='ls -CF'
alias la='ls -Alh' # show hidden files
alias ls='ls -aFh --color=always' # add colors and file type extensions
alias lx='ls -lXBh' # sort by extension
alias lk='ls -lSrh' # sort by size
alias lc='ls -lcrh' # sort by change time
alias lu='ls -lurh' # sort by access time
alias lr='ls -lRh' # recursive ls
alias lt='ls -ltrh' # sort by date
alias lm='ls -alh |more' # pipe through 'more'
alias lw='ls -xAh' # wide listing format
alias ll='ls -Fls' # long listing format
alias labc='ls -lap' #alphabetical sort
alias lf="ls -l | egrep -v '^d'" # files only
alias ldir="ls -l | egrep '^d'" # directories only

# Remove a directory and all files
alias rmd='/bin/rm  --recursive --force --verbose '

# Change directory aliases
#alias home='cd ~'
#alias cd..='cd ..'
#alias ..='cd ..'
#alias ...='cd ../..'
#alias ....='cd ../../..'
#alias .....='cd ../../../..'

# cd into the old directory
alias bd='cd "$OLDPWD"'

# Grep coloring
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# Modified commands
#alias cp='cp -i'
#alias rm='rm -iv'
#alias mv='mv -i'
alias ping='ping -c 10'
alias less='less -R'
alias cls='clear'
#alias tmux="tmux -2"
alias tracert='echo "Use mtr, you ninny."'

# Search command line history
alias hisearch='history | grep '

# Show the date
alias da='date "+%Y-%m-%d %A %T %Z"'

# Safe and forced reboots
alias rebootsafe='sudo shutdown -r now'
alias rebootforce='sudo shutdown -r -n now'
alias rebootin='sudo shutdown -r'

# Checksums
alias sha='openssl sha'
alias sha1='openssl sha1'
alias md5='openssl md5'
alias rmd160='openssl rmd160'

# Easily manage clipboard in shell
# For example, a file: cat somefile | pbcopy
# Dump clipboard to file? pbpaste > somefile
alias pbcopy='xsel -b'
alias pbpaste='xsel -b'

# shows "git diff" across any project in any subdirectory
alias git-differ='for g in $(find . -name ".git"); do g=${g%%.git};printf "$g\t\t\t";pu $g >/dev/null && git diff |wc -l; p >/dev/null; done'

# does git house keeping across any project in any subdirectory
alias git-housekeep='for g in $(find . -name ".git"); do g=${g%%.git};echo $g;pu $g && git repack && git gc --auto && p;done'

# Make SSH automatically complete the hostname you ssh to (if it's in your config or history)
#complete -o default -o nospace -W "$(/usr/bin/env ruby -ne 'puts $_.split(/[,\s]+/)[1..-1].reject{|host| host.match(/\*|\?/)} if $_.match(/^\s*Host\s+/);' < $HOME/.ssh/config)" scp sftp ssh

# Sometimes you're just not sure you want to delete something...
alias trash='mv -t ~/.local/share/Trash/files --backup=t'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
#alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Reload .bashrc
alias reloadbash='source ~/.bashrc'

# One alias to rule them all (which is a function)
myaliases ()
{
  printf "My Aliases (GLOBAL)\n"
  grep '^alias' $HOME/.bashrc
  if [[ -f "$HOME/.aliases" ]]
  then
    printf "\nMy Aliases (LOCAL)\n"
	grep '^alias' $HOME/.aliases
  fi
}

#-------------------------------------------------------------#
# Load local aliases                                          #
#-------------------------------------------------------------#
if [[ -f "$HOME/.aliases" ]]
then
  source "$HOME/.aliases"
fi

#-------------------------------------------------------------#
# PATH settings                                               #
#-------------------------------------------------------------#
export PATH="$PATH:/sbin:/usr/sbin:/usr/local/sbin:$HOME/bin"

#-------------------------------------------------------------#
# My freaking awesome prompt                                  #
#-------------------------------------------------------------#
export PS1="\n\[\033[1;37m\]\342\224\214($(if [[ ${EUID} == 0 ]]; then echo '\[\033[01;31m\]\h'; else echo '\[\033[01;34m\]\u@\h'; fi)\[\033[1;37m\])\342\224\200(\$(if [[ \$? == 0 ]]; then echo \"\[\033[01;32m\]\342\234\223\"; else echo \"\[\033[01;31m\]\342\234\227\"; fi)\[\033[1;37m\])\342\224\200(\[\033[1;34m\]\A \d\[\033[1;37m\])\[\033[1;37m\]\n\342\224\224\342\224\200(\[\033[1;32m\]\w\[\033[1;37m\])\342\224\200(\[\033[1;32m\]\$(ls -1 | wc -l | sed 's: ::g') files, \$(ls -sh | head -n1 | sed 's/total //')b\[\033[1;37m\])\342\224\200\342\225\274 \[\033[0m\]"

#-------------------------------------------------------------#
# Better man pages (if possible)                              #
#-------------------------------------------------------------#
# Color for manpages in less makes manpages a little easier to read
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;33m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m'
if command -v most > /dev/null 2>&1
then
    export PAGER="most -s"
fi

#-------------------------------------------------------------#
# Less improvement                                            #
#-------------------------------------------------------------#
# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

#-------------------------------------------------------------#
# ARCHIVE handling                                            #
#-------------------------------------------------------------#
extract()
{
  if [ -z ${1} ] || [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
    echo "Usage: extract <archive> [directory]"
    echo "Example: extract presentation.zip."
    echo "Valid archive types are:"
    echo "tar.bz2, tar.gz, tar.xz, tar, bz2, gz, tbz2,"
    echo "tbz, tgz, lzo, rar, zip, 7z, xz, txz, lzma and tlz"
  else
    case "$1" in
      *.tar.bz2|*.tbz2|*.tbz)         tar xvjf "$1" ;;
      *.tgz)                          tar zxvf "$1" ;;
      *.tar.gz)                       tar xvzf "$1" ;;
      *.tar.xz)                       tar xvJf "$1" ;;
      *.tar)                          tar xvf "$1" ;;
      *.rar)                          7z x "$1" ;;
      *.zip)                          unzip "$1" ;;
      *.7z)                           7z x "$1" ;;
      *.lzo)                          lzop -d  "$1" ;;
      *.gz)                           gunzip "$1" ;;
      *.bz2)                          bunzip2 "$1" ;;
      *.Z)                            uncompress "$1" ;;
      *.xz|*.txz|*.lzma|*.tlz)        xz -d "$1" ;;
      *) echo "Sorry, '$1' could not be decompressed." ;;
    esac
  fi
}

# Alias's for archives (first tar.file.name then files_to_tar)
alias mktar='tar -cvf'
alias mkbz2='tar -cvjf'
alias mkgz='tar -cvzf'
alias mkxz='tar -cJf'

#-------------------------------------------------------------#
# FTEXT                                                       #
#-------------------------------------------------------------#
# Searches for text in all files in the current folder
ftext ()
{
	# -i case-insensitive
	# -I ignore binary files
	# -H causes filename to be printed
	# -r recursive search
	# -n causes line number to be printed
	# optional: -F treat search term as a literal, not a regular expression
	# optional: -l only print filenames and not the matching lines ex. grep -irl "$1" *
	grep -iIHrn --color=always "$1" . | less -r
}

#-------------------------------------------------------------#
# Directory workflow                                          #
#-------------------------------------------------------------#
# Copy and go to the directory
cpg ()
{
	if [ -d "$2" ];then
		cp $1 $2 && cd $2
	else
		cp $1 $2
	fi
}

# Move and go to the directory
mvg ()
{
	if [ -d "$2" ];then
		mv $1 $2 && cd $2
	else
		mv $1 $2
	fi
}

# Create and go to the directory
mkdirg ()
{
	mkdir -p $1
	cd $1
}

# Goes up a specified number of directories  (i.e. up 4)
up ()
{
	local d=""
	limit=$1
	for ((i=1 ; i <= limit ; i++))
		do
			d=$d/..
		done
	d=$(echo $d | sed 's/^\///')
	if [ -z "$d" ]; then
		d=..
	fi
	cd $d
}

#-------------------------------------------------------------#
# Trim                                                        #
#-------------------------------------------------------------#
# Trim leading and trailing spaces (for scripts)
trim()
{
	local var=$@
	var="${var#"${var%%[![:space:]]*}"}"  # remove leading whitespace characters
	var="${var%"${var##*[![:space:]]}"}"  # remove trailing whitespace characters
	echo -n "$var"
}

#-------------------------------------------------------------#
# rot13                                                       #
#-------------------------------------------------------------#
# Encrypt & decrypt with rot13
rot13 () {
	if [ $# -eq 0 ]; then
		tr '[a-m][n-z][A-M][N-Z]' '[n-z][a-m][N-Z][A-M]'
	else
		echo $* | tr '[a-m][n-z][A-M][N-Z]' '[n-z][a-m][N-Z][A-M]'
	fi
}

#-------------------------------------------------------------#
# Remove python compiled files recursively                    #
#-------------------------------------------------------------#
pyclean ()
{
  find . -name '*.pyc' -delete
}

#-------------------------------------------------------------#
# SSH AGENT                                                   #
#-------------------------------------------------------------#·
if [[ -e "$HOME/.ssh/id_ed25519" ]]; then
  if ! pgrep -u "$USER" ssh-agent > /dev/null; then
    ssh-agent >| ~/.ssh-agent-thing
  fi
  if [[ ! "$SSH_AUTH_SOCK" ]]; then
    eval "$(<~/.ssh-agent-thing)"
  fi
else
  if [[ -e "$HOME/.ssh-agent-thing" ]]; then
    killall --user "$USER" ssh-agent
    rm -rf "$HOME/.ssh-agent-thing"
  fi
fi

#-------------------------------------------------------------#
# TODO                                                        #
#-------------------------------------------------------------#

#if [ -f ~/.bash_private.gpg ]; then
#   eval "$(gpg --decrypt ~/.bash_private.gpg 2>/dev/null)"
#fi
#alias MYsql='mysql -uadmin -psecret'
#wglatest(){ wget -O https://admin:secret@server.com/latest; }
# Source https://serverfault.com/questions/3743/what-useful-things-can-one-add-to-ones-bashrc

# smart advanced completion, download from
# http://bash-completion.alioth.debian.org/
#if [[ -f $HOME/local/bin/bash_completion ]]; then
#    . $HOME/local/bin/bash_completion
#fi

#-------------------------------------------------------------#
# END BASHRC                                                  #
#-------------------------------------------------------------#
